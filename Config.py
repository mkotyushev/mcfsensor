import json

from PyQt5.QtCore import QObject


class Config(QObject):
    def __init__(self, file_name=None, parent=None):
        super().__init__(parent)

        self.nodes_configs = {}
        self.fbgs_configs = {}

        if file_name is None:
            return

        f = open(file_name)
        dictionary = json.load(f)
        f.close()

        self.nodes_configs = string_keys_to_int(dictionary["nodes"])
        self.fbgs_configs = string_keys_to_int(dictionary["fbgs"])


def string_keys_to_int(dictionary):
    result = {}
    for key, value in dictionary.items():
        if key.isdigit():
            result[int(key)] = value

    return result
