from PyQt5.QtWidgets import QDialog
from windows.uis.calibrationmenu import Ui_Dialog


class CalibrationMenu(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
