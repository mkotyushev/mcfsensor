from PyQt5.QtWidgets import QDialog
from Windows.UIs.CalibrationStrainUI import Ui_CalibrationStrainUI


class CalibrationStrain(QDialog):
    def __init__(self, config, parent=None):
        super().__init__(parent)
        self.ui = Ui_CalibrationStrainUI()
        self.ui.setupUi(self)

        self.setFixedSize(self.geometry().width(), self.geometry().height())

        self.config = config

        self.accepted.connect(self.on_accept)

    def on_accept(self):
        self.config.sync()
        self.close()
