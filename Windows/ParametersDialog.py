from PyQt5.QtWidgets import QDialog
from windows.uis.parametersdialog import Ui_Dialog


class ParametersDialog(QDialog):
    def __init__(self, parent=None):
        QDialog.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
