from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import QSettings

from Windows.UIs.ConfiguratorUI import Ui_ConfiguratorUI
from Windows.CalibrationThermal import CalibrationThermal
from Windows.CalibrationStrain import CalibrationStrain


class Configurator(QDialog):
    def __init__(self, config, parent=None):
        super().__init__(parent)
        self.ui = Ui_ConfiguratorUI()
        self.ui.setupUi(self)

        self.setWindowTitle("Конфигуратор")
        self.setFixedSize(self.geometry().width(), self.geometry().height())

        self.result = config

        self.config = QSettings("temp.ini", QSettings.IniFormat)
        for key in self.result.allKeys():
            value = self.result.value(key, 0)
            self.config.setValue(key, value)
        self.config.sync()

        self.ui.comboBox_fbg_fbg.currentIndexChanged.connect(self.load_fbg)
        self.ui.pushButton_fbg_add.clicked.connect(self.on_fbg_add)
        self.ui.pushButton_fbg_remove.clicked.connect(self.on_fbg_remove)
        self.ui.pushButton_fbg_calibrateStrain.clicked.connect(self.on_calibrate_strain)
        self.ui.pushButton_fbg_calibrateThemnal.clicked.connect(self.on_calibrate_thermal)
        self.ui.pushButton_common_calibrateAllStrain.clicked.connect(self.on_calibrate_all_strain)
        self.ui.pushButton_common_calibrateAllThemnal.clicked.connect(self.on_calibrate_all_thermal)
        self.ui.lineEdit_fbg_r.textEdited.connect(lambda: self.on_updated(self.ui.lineEdit_fbg_r))
        self.ui.lineEdit_fbg_phi.textEdited.connect(lambda: self.on_updated(self.ui.lineEdit_fbg_phi))
        self.ui.lineEdit_fbg_wl.textEdited.connect(lambda: self.on_updated(self.ui.lineEdit_fbg_wl))
        self.ui.lineEdit_fbg_ks.textEdited.connect(lambda: self.on_updated(self.ui.lineEdit_fbg_ks))
        self.ui.lineEdit_fbg_kt.textEdited.connect(lambda: self.on_updated(self.ui.lineEdit_fbg_kt))

        self.ui.comboBox_node_node.currentIndexChanged.connect(self.load_node)
        self.ui.pushButton_node_add.clicked.connect(self.on_node_add)
        self.ui.pushButton_node_remove.clicked.connect(self.on_node_remove)
        self.ui.lineEdit_node_offset.textEdited.connect(lambda: self.on_updated(self.ui.lineEdit_node_offset))
        self.ui.comboBox_node_fbg_1.currentIndexChanged.connect(lambda: self.on_updated(self.ui.comboBox_node_fbg_1))
        self.ui.comboBox_node_fbg_2.currentIndexChanged.connect(lambda: self.on_updated(self.ui.comboBox_node_fbg_2))
        self.ui.comboBox_node_fbg_t.currentIndexChanged.connect(lambda: self.on_updated(self.ui.comboBox_node_fbg_t))

        self.accepted.connect(self.on_accept)
        self.destroyed.connect(self.on_destroy)

        self.init()

    def init(self):
        # Load config nodes and fbgs
        keys = self.config.childGroups()
        nodes = [key for key in keys if key.startswith("node")]
        fbgs = [key for key in keys if key.startswith("fbg")]

        for fbg in fbgs:
            # Load new fbg name from config to combo
            number = fbg.split("fbg")[1]
            self.ui.comboBox_fbg_fbg.addItem(str(number))
            self.ui.comboBox_node_fbg_1.addItem(str(number))
            self.ui.comboBox_node_fbg_2.addItem(str(number))
            self.ui.comboBox_node_fbg_t.addItem(str(number))

        for node in nodes:
            # Load new node name from config to combo
            number = node.split("node")[1]
            self.ui.comboBox_node_node.addItem(str(number))

    def load_fbg(self):
        fbg = self.ui.comboBox_fbg_fbg.currentIndex()
        if fbg == -1:
            return

        key = "fbg{0}".format(fbg)
        r = self.config.value(key + "/" + "r", 0)
        self.ui.lineEdit_fbg_r.setText(str(r))
        phi = self.config.value(key + "/" + "phi", 0)
        self.ui.lineEdit_fbg_phi.setText(str(phi))
        wl = self.config.value(key + "/" + "wl", 0)
        self.ui.lineEdit_fbg_wl.setText(str(wl))
        kt = self.config.value(key + "/" + "kt", 0)
        self.ui.lineEdit_fbg_kt.setText(str(kt))
        ks = self.config.value(key + "/" + "ks", 0)
        self.ui.lineEdit_fbg_ks.setText(str(ks))

    def load_node(self):
        node = self.ui.comboBox_node_node.currentIndex()
        if node == -1:
            return

        key = "node{0}".format(node)
        offset = int(self.config.value(key + "/" + "offset", 0))
        self.ui.lineEdit_node_offset.setText(str(offset))
        fbg = int(self.config.value(key + "/" + "fbg_1", "0"))
        self.ui.comboBox_node_fbg_1.setCurrentIndex(fbg)
        fbg = int(self.config.value(key + "/" + "fbg_2", "0"))
        self.ui.comboBox_node_fbg_2.setCurrentIndex(fbg)
        fbg = int(self.config.value(key + "/" + "fbg_t", "0"))
        self.ui.comboBox_node_fbg_t.setCurrentIndex(fbg)

    def on_updated(self, widget):
        widget_type, parameter_module, parameter_name = widget.objectName().split("_", 2)

        key, value = None, None

        if widget_type == "lineEdit":
            value = float(widget.text())
        elif widget_type == "comboBox":
            value = int(widget.currentIndex())

        if parameter_module == "fbg":
            fbg = int(self.ui.comboBox_fbg_fbg.currentIndex())
            if fbg != -1:
                key = "fbg{0}/{1}".format(fbg, parameter_name)
        elif parameter_module == "node":
            node = int(self.ui.comboBox_node_node.currentIndex())
            if node != -1:
                key = "node{0}/{1}".format(node, parameter_name)

        if key is None or value is None:
            return

        self.config.setValue(key, value)

    # def on_fbg_r_updated(self):
    #     fbg = int(self.ui.comboBox_fbg_fbg.currentIndex())
    #     if fbg != -1:
    #         key = "fbg{0}/r".format(fbg)
    #         value = float(self.ui.lineEdit_fbg_r.text())
    #         self.config.setValue(key, value)
    #
    # def on_fbg_phi_updated(self):
    #     fbg = int(self.ui.comboBox_fbg_fbg.currentIndex())
    #     if fbg != -1:
    #         key = "fbg{0}/phi".format(fbg)
    #         value = int(self.ui.lineEdit_fbg_phi.text())
    #         self.config.setValue(key, value)
    #
    # def on_fbg_wl_updated(self):
    #     fbg = int(self.ui.comboBox_fbg_fbg.currentIndex())
    #     if fbg != -1:
    #         key = "fbg{0}/wl".format(fbg)
    #         value = int(self.ui.lineEdit_fbg_wl.text())
    #         self.config.setValue(key, value)
    #
    # def on_fbg_kt_updated(self):
    #     fbg = int(self.ui.comboBox_fbg_fbg.currentIndex())
    #     if fbg != -1:
    #         key = "fbg{0}/kt".format(fbg)
    #         value = float(self.ui.lineEdit_fbg_kt.text())
    #         self.config.setValue(key, value)
    #
    # def on_fbg_ks_updated(self):
    #     fbg = int(self.ui.comboBox_fbg_fbg.currentIndex())
    #     if fbg != -1:
    #         key = "fbg{0}/ks".format(fbg)
    #         value = float(self.ui.lineEdit_fbg_ks.text())
    #         self.config.setValue(key, value)
    #
    # def on_node_offset_updated(self):
    #     node = self.ui.comboBox_node_node.currentIndex()
    #     if node != -1:
    #         key = "node{0}/offset".format(node)
    #         value = float(self.ui.lineEdit_node_offset.text())
    #         self.config.setValue(key, value)
    #
    # def on_node_fbg_1_updated(self):
    #     node = self.ui.comboBox_node_node.currentIndex()
    #     if node != -1:
    #         key = "node{0}/fbg_1".format(node)
    #         fbg = int(self.ui.comboBox_node_fbg_1.currentIndex())
    #         if fbg != -1:
    #             value = "fbg{0}".format(fbg)
    #             self.config.setValue(key, value)
    #
    # def on_node_fbg_2_updated(self):
    #     node = self.ui.comboBox_node_node.currentIndex()
    #     if node != -1:
    #         key = "node{0}/fbg_2".format(node)
    #         fbg = int(self.ui.comboBox_node_fbg_2.currentIndex())
    #         if fbg != -1:
    #             value = "fbg{0}".format(fbg)
    #             self.config.setValue(key, value)
    #
    # def on_node_fbg_t_updated(self):
    #     node = self.ui.comboBox_node_node.currentIndex()
    #     if node != -1:
    #         key = "node{0}/fbg_t".format(node)
    #         fbg = int(self.ui.comboBox_node_fbg_t.currentIndex())
    #         if fbg != -1:
    #             value = "fbg{0}".format(fbg)
    #             self.config.setValue(key, value)

    def on_node_add(self):
        number = self.ui.comboBox_node_node.count()
        self.ui.comboBox_node_node.addItem(str(number))

        self.update()

    def on_node_remove(self):
        # TODO: reformat list of nodes on node removing in order to prevent "1 2 4" situations when removing 3.
        number = self.ui.comboBox_node_node.currentIndex()
        self.ui.comboBox_node_node.removeItem(number)
        key = "node{0}".format(number)

        self.config.remove(key)
        self.config.sync()

    def on_fbg_add(self):
        number = self.ui.comboBox_fbg_fbg.count()
        self.ui.comboBox_fbg_fbg.addItem(str(number))
        self.ui.comboBox_node_fbg_1.addItem(str(number))
        self.ui.comboBox_node_fbg_2.addItem(str(number))
        self.ui.comboBox_node_fbg_t.addItem(str(number))

        self.update()

    def on_fbg_remove(self):
        # TODO: reformat list of fbgs on node removing in order to prevent "1 2 4" situations when removing 3.
        number = self.ui.comboBox_fbg_fbg.currentIndex()

        if number == -1:
            return

        key = "fbg{0}".format(number)

        self.config.remove(key)
        self.config.sync()

        self.ui.comboBox_fbg_fbg.removeItem(number)
        self.ui.comboBox_node_fbg_1.removeItem(number)
        self.ui.comboBox_node_fbg_2.removeItem(number)
        self.ui.comboBox_node_fbg_t.removeItem(number)

        self.update()

    def on_accept(self):
        self.result.clear()

        for key in self.config.allKeys():
            value = self.config.value(key, 0)
            self.result.setValue(key, value)

        self.result.sync()

        self.config.clear()
        self.config.sync()

    def on_destroy(self):
        self.config.clear()
        self.config.sync()

    def on_cancel(self):
        self.config.clear()
        self.config.sync()

    def on_calibrate_thermal(self):
        dialog = CalibrationThermal(config=self.config, parent=self)
        dialog.exec()

    def on_calibrate_strain(self):
        dialog = CalibrationStrain(config=self.config, parent=self)
        dialog.exec()

    def on_calibrate_all_thermal(self):
        dialog = CalibrationThermal(config=self.config, parent=self)
        dialog.exec()

    def on_calibrate_all_strain(self):
        dialog = CalibrationStrain(config=self.config, parent=self)
        dialog.exec()


if __name__ == '__main__':
    try:
        import sys
        import traceback
        from PyQt5.QtWidgets import QApplication
        from PyQt5.QtCore import QSettings, QCoreApplication

        settings = QSettings("test.ini", QSettings.IniFormat)

        app = QApplication(sys.argv)
        window = Configurator(settings)
        window.show()
        sys.exit(app.exec_())
    except Exception as e:
        traceback.print_exc()
