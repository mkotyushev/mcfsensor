# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\mkotyushev\Google Диск\4 курс\Диплом\ui\mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(524, 462)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.plotLayout = QtWidgets.QGridLayout()
        self.plotLayout.setSizeConstraint(QtWidgets.QLayout.SetMaximumSize)
        self.plotLayout.setObjectName("plotLayout")
        self.gridLayout.addLayout(self.plotLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 524, 21))
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.sensorParamsAction = QtWidgets.QAction(MainWindow)
        self.sensorParamsAction.setObjectName("sensorParamsAction")
        self.writeLogAction = QtWidgets.QAction(MainWindow)
        self.writeLogAction.setObjectName("writeLogAction")
        self.closeAction = QtWidgets.QAction(MainWindow)
        self.closeAction.setObjectName("closeAction")
        self.menu.addAction(self.sensorParamsAction)
        self.menu.addAction(self.writeLogAction)
        self.menu.addSeparator()
        self.menu.addAction(self.closeAction)
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.menu.setTitle(_translate("MainWindow", "Настройки"))
        self.sensorParamsAction.setText(_translate("MainWindow", "Параметры сенсора"))
        self.writeLogAction.setText(_translate("MainWindow", "Писать в файл"))
        self.closeAction.setText(_translate("MainWindow", "Закрыть"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

