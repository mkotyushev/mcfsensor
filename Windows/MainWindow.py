from PyQt5.QtCore import QThread, QSettings, QVariant, QTimer, Qt
from PyQt5.QtWidgets import QMainWindow
from PyQt5.QtChart import QChart, QChartView

import numpy as np

from Interrogator.Interrogator import Interrogator
from Analyzer.SpectrumAnalyzer import SpectrumAnalyzer
from Sensor.Sensor import Sensor
from Visualization.Interpolator import Interpolator
from Visualization.PainterPyQtGraph import PainterPyQtGraph

from Configurator.Config import Config
from Windows.UIs.MainWindowUI import Ui_MainWindow
import Analyzer.PrecisePeakSearchers as pps
import Analyzer.RudePeaksSearchers as rps


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        # Create and load settings
        self.settings = QSettings("test.ini", QSettings.IniFormat)

        # Handle threads
        self.threads = []
        self.destroyed.connect(self.kill_threads)

        # Add painter
        self.painter = PainterPyQtGraph(parent=None)
        # self.add_thread([self.painter])
        self.ui.gridLayout_3DPlot.addWidget(self.painter)

        # Add interpolator
        self.interpolator = Interpolator(parent=None)
        self.add_thread([self.interpolator])
        self.interpolator.updated.connect(self.painter.update_data, Qt.QueuedConnection)

        # Add sensor
        config = Config("Configurator/Configs/config.json")
        self.sensor = Sensor(
            parent=None,
            nodes_configs=config.nodes_configs,
            cores_configs=config.cores_configs,
            fbgs_configs=config.fbgs_configs)
        self.add_thread([self.sensor])
        self.sensor.updated.connect(self.interpolator.update, Qt.QueuedConnection)

        # Add analyzer
        params = dict()
        params["peak_number"] = 1
        params["ban_widths"] = np.full(shape=1, fill_value=10)
        params["fwhms"] = np.array(
            [
                5e-1
            ]
        )

        self.analyzer = SpectrumAnalyzer(
            parent=None,
            rps=rps.LocalMaximaRPS(),
            pps=pps.Poly2ApproximationPPS(),
            matcher=None,
            parameters=params
        )
        self.add_thread([self.analyzer])
        self.analyzer.updated.connect(self.sensor.update, Qt.QueuedConnection)

        # Add interrogator
        self.interrogator = Interrogator(
            parent=None,
            host="10.6.1.10",
            command_port=3500,
            data_port=3365,
            long_timeout=1000,
            short_timeout=500
        )
        self.add_thread([self.interrogator])

        self.ui.pushButton_interrogatorInterrogate.pressed.connect(
            lambda: self.interrogator.acquise(self.ui.comboBox_interrogatorChannelNumber.currentData()))
        self.interrogator.received.connect(self.analyzer.update, Qt.QueuedConnection)

        # Add timer
        self.timer = QTimer()
        self.timer.setInterval(1000)
        self.timer.timeout.connect(self.interrogator.acquise_continious, Qt.QueuedConnection)
        self.timer.start()

        self.init_ui()

        self.start_threads()

        print(int(QThread.currentThreadId()), __name__)

    def init_ui(self):
        # синхронизовать выбор сердцевины на трех виджетах
        self.ui.tableWidget_wavelength_all.cellClicked.connect(self.ui.tableWidget_wavelength_selected.setCurrentCell)
        self.ui.tableWidget_wavelength_all.cellClicked.connect(
            lambda x, y: self.ui.tabWidget_spectra.setCurrentIndex(x))
        self.ui.tableWidget_wavelength_selected.cellClicked.connect(self.ui.tableWidget_wavelength_all.setCurrentCell)
        self.ui.tableWidget_wavelength_selected.cellClicked.connect(
            lambda x, y: self.ui.tabWidget_spectra.setCurrentIndex(x))
        self.ui.tabWidget_spectra.currentChanged.connect(
            lambda x: self.ui.tableWidget_wavelength_all.setCurrentCell(x, 0))
        self.ui.tabWidget_spectra.currentChanged.connect(
            lambda x: self.ui.tableWidget_wavelength_selected.setCurrentCell(x, 0))
        core_count = self.settings.value("common/core_count", 7)

        # добавить строк в таблицы по числу сердцевин
        for i in range(core_count):
            self.ui.tableWidget_wavelength_all.insertRow(self.ui.tableWidget_wavelength_all.rowCount())
        self.ui.tableWidget_wavelength_all.insertColumn(self.ui.tableWidget_wavelength_all.columnCount())
        for i in range(core_count):
            self.ui.tableWidget_wavelength_selected.insertRow(self.ui.tableWidget_wavelength_selected.rowCount())
        self.ui.tableWidget_wavelength_selected.insertColumn(self.ui.tableWidget_wavelength_selected.columnCount())

        # добавить табов для спектров по числу сердцевин
        for i in range(core_count):
            chart = QChart()
            tab = QChartView(chart, self.ui.tabWidget_spectra)
            self.ui.tabWidget_spectra.insertTab(self.ui.tabWidget_spectra.count(), tab, "Core {0}".format(i + 1))

        # добавить строк в комбо по числу сердцевин
        for i in range(8):
            self.ui.comboBox_interrogatorChannelNumber.addItem("{0}".format(i), QVariant(i))

    def set_config(self, config):
        """
        Create new sensor with new config.
        :param config: -- config for sensor.
        :return: None
        """
        self.sensor = Sensor(
            nodes_configs=config.nodes_configs,
            fbgs_configs=config.fbgs_configs,
            parent=self)
        self.sensor.updated.connect(self.interpolator.update)

    def add_thread(self, objects):
        """
        Creates thread, adds it into to-destroy list and moves objects to it. Thread is QThread there.
        :param objects -- list of QObjects.
        :return None
        """
        # Create new thread
        thread = QThread()
        # Move objects to new thread.
        for obj in objects:
            obj.moveToThread(thread)

        # Add new thread to threads list
        self.threads.append(thread)

    def start_threads(self):
        for thread in self.threads:
            thread.start()

    def kill_threads(self):
        """
        Closes all of created threads for this window.
        :return: None
        """
        # Iterate over all the threads and call wait() and quit().
        for thread in self.threads:
            thread.wait()
            thread.quit()

    # def keyPressEvent(self, event):
    #     print("Key was pressed", self.__class__.__name__)
    #     self.analyzer.update()


if __name__ == "__main__":
    pass
