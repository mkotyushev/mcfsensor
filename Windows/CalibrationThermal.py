from PyQt5.QtWidgets import QDialog
from Windows.UIs.CalibrationThermalUI import Ui_CalibrationThermalUI


class CalibrationThermal(QDialog):
    def __init__(self, config, parent=None):
        super().__init__(parent)
        self.ui = Ui_CalibrationThermalUI()
        self.ui.setupUi(self)

        self.setFixedSize(self.geometry().width(), self.geometry().height())

        self.config = config

        self.accepted.connect(self.on_accept)

    def on_accept(self):
        self.config.sync()
        self.close()
