import Analyzer.Spectra as spectra
import numpy as np


class LocalMaximaRPS:  # rude peaks searcher
    def __call__(self, xs, ys, n, widths):
        mask = np.full(shape=xs.shape, fill_value=True)

        indices = np.array([], dtype=np.int64)
        for i in range(n):
            indices = np.append(indices, spectra.argmax_masked(ys, ~mask))
            mask &= ~spectra.where_domain(xs, xs[indices[i]], widths[i])

        indices = indices[indices.argsort()[::-1]]

        return xs[indices], ys[indices]
