#!/usr/bin/env python3

import re
import sys
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt

from scipy import signal
from os import listdir
from os.path import isfile, join


# TODO:
# * Test alghorithm on spectrum with damaged gratings.
# * Debug parsing of arguments.

def peakShift(ys_prev, ys_curr, peaks_x, peaks_y):
    """Return position of peak which was shifted"""
    deviation = abs(ys_curr - ys_prev)
    # TODO: add calculation of avg distance between peaks
    # to remove hardcoded value of pulse length in convolve
    mov_avg = signal.convolve(deviation, np.ones(200), 'same')
    maximum = ys_prev[mov_avg.argmax()]
    nearest_peak = peaks_x[np.array(abs(peaks_y - maximum)).argmin()]

    return nearest_peak


def weightedMovingAverage(x, y, step_size=0.5, width=20):
    bin_centers = np.arange(np.min(x), np.max(x) - 0.5 * step_size, step_size) + 0.5 * step_size
    bin_avg = np.zeros(len(bin_centers))

    # We're going to weight with a Gaussian function
    def gaussian(x, amp=1, mean=0, sigma=1):
        return amp * np.exp(-(x - mean) ** 2 / (2 * sigma ** 2))

    for index in range(0, len(bin_centers)):
        bin_center = bin_centers[index]
        weights = gaussian(x, mean=bin_center, sigma=width)
        bin_avg[index] = np.average(y, weights=weights)

    return (bin_centers, bin_avg)


def Smooth(y, box_pts):
    box = np.ones(box_pts) / box_pts
    y_smooth = np.convolve(y, box, mode='same')
    return y_smooth


def getAverage(x, y):
    if len(x) != len(y):
        return x, y
    length = len(x)
    result_x = np.array([])
    result_y = np.array([])
    y_curr = np.array([y[0]])
    for i in range(length):
        curr = i
        next = i + 1
        if next >= length:
            break
        if x[curr] != x[next]:
            result_x = np.append(result_x, x[curr])
            result_y = np.append(result_y, np.average(y_curr))
            y_curr = np.array([y[next]])
        else:
            y_curr = np.append(y_curr, y[next])
        curr = next
        next = next + 1
    return result_x, result_y


def getFWHM(x, y, peak_y):
    """ Works for single peak """
    yHM = (peak_y / 2)
    indices = np.where(y > yHM)

    idx = indices[0][0] - 1
    if idx < 0:
        idx = 0
    x1 = x[idx]
    y1 = y[idx]

    idx = indices[0][0]
    x2 = x[idx]
    y2 = y[idx]
    if 0 != (y1 - y2):
        x_left = x1 + (x2 - x1) / (y2 - y1) * (yHM - y1)
    else:
        x_left = x1

    idx = indices[0][-1] + 1
    if idx >= len(x):
        idx = len(x) - 1
    x1 = x[idx]
    y1 = y[idx]

    idx = indices[0][-1]
    x2 = x[idx]
    y2 = y[idx]
    if 0 != (y1 - y2):
        x_right = x1 + (x2 - x1) / (y2 - y1) * (yHM - y1)
    else:
        x_right = x2

    return x_left, x_right


# returns index of array element nearest from value
def findNearestIdx(array, value):
    return (np.abs(array - value)).argmin()


# correct indices left & right to be in boards of array with len = lengh
def fetchBoards(left, right, length):
    l = left
    r = right
    if l < 0:
        l = 0
    if r > length - 1:
        r = length - 1
    return l, r


# returns np.poly1d degree of 2 which best approximates y in present area
def approx(xs, ys, x_start, x_stop):
    mask = (xs >= x_start) & (xs < x_stop)
    return np.poly1d(np.polyfit(xs[mask], ys[mask], 2))


# returns index of maximal element in masked array
def argmax_masked(array, mask):
    arr = ma.array(array, dtype=np.float64, mask=mask, fill_value=-np.inf)
    return np.argmax(arr)


def where_domain(xs, x, width):
    """
    Get mask of domain [x - width : x + width).
    :param xs: np.array<np.float64> -- data
    :param x: np.float64 -- central point of domain
    :param width: np.float64 -- radius of domain
    :return: np.array<boolean> -- mask of domain [x - width : x + width)
    """
    return (xs >= (x - width)) & (xs < (x + width))


def maxima_indices(xs, ys, peaks_num, peak_widths):
    """
    Find indices of peak_num peaks with interpeak distances from peak_widths.
    :param xs:np.array<np.float64> -- x data
    :param ys:np.array<np.float64> -- y data
    :param peaks_num:int -- the number of peaks to search
    :param peak_widths:<np.float64> -- widths of each peak
        (to avoid spurious peak detection in vicinity of already found peaks)
    :return: indices:np.array<int> -- indices of maxima
    """
    mask = np.full(shape=xs.shape, fill_value=True)

    indices = np.array([], dtype=np.int64)
    for i in range(peaks_num):
        indices = np.append(indices, argmax_masked(ys, ~mask))
        mask &= ~where_domain(xs, xs[indices[i]], peak_widths[i])

    indices = indices[indices.argsort()[::-1]]

    return indices
