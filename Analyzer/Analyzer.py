from PyQt5.QtCore import QObject, pyqtSignal


class Analyzer(QObject):
    updated = pyqtSignal(int, object, object)

    def __init__(self, parent=None):
        super().__init__(parent)
