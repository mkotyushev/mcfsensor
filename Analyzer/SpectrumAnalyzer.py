from PyQt5.QtCore import QThread

from Analyzer.Analyzer import Analyzer

import numpy as np


# TODO: add matcher -- class to match wavelength and fbg with known initial wavelength
class SpectrumAnalyzer(Analyzer):
    def __init__(self, rps, pps, matcher, parameters, parent=None):
        super().__init__(parent)

        self.rps = rps
        self.pps = pps
        self.matcher = matcher

        self.parameters = parameters

        self.xs_max = None
        self.ys_max = None

    def update(self, ch_num, xs, ys) -> None:
        print(int(QThread.currentThreadId()), __name__)

        # find the maxima as local maxima (a low accuracy)
        xs_max_rude, ys_max_rude = self.rps(xs, ys, self.parameters["peak_number"], self.parameters["ban_widths"])

        # find approximation in areas
        xs_max, ys_max = np.array([]), np.array([])
        for x_rude, width in zip(xs_max_rude, self.parameters["fwhms"]):
            mask = (xs >= x_rude - width) & (xs < x_rude + width)
            x_max, y_max = self.pps(xs[mask], ys[mask])

            xs_max = np.append(xs_max, x_max)
            ys_max = np.append(ys_max, y_max)

        self.xs_max = xs_max
        self.ys_max = ys_max

        self.updated.emit(ch_num, self.xs_max, self.ys_max)


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    import Analyzer.PrecisePeakSearchers as pps
    import Analyzer.RudePeaksSearchers as rps

    class CONSTS:
        class DRAWING:
            WIDTH = 1e-9
            HEIGHT = 1

        class SPECTRA:
            INTERPEAK_DISTANCE = 4

            class PEAK:
                FWHM = 3e-1

    load_from = "interrogator"
    if load_from == "file":
        filepath = "SpectrumAnalyzerTestData"

        data = np.loadtxt(filepath, delimiter=' ')
        xs, ys = data[0], data[1]

        plt.plot(xs, ys, "b-")

        # params
        params = {}
        params["peak_number"] = 9
        params["ban_widths"] = np.full(shape=9, fill_value=CONSTS.SPECTRA.INTERPEAK_DISTANCE / 2)
        params["fwhms"] = np.array(
            [
                2.0 * CONSTS.SPECTRA.PEAK.FWHM,
                2.0 * CONSTS.SPECTRA.PEAK.FWHM,
                2.0 * CONSTS.SPECTRA.PEAK.FWHM,
                1.0 * CONSTS.SPECTRA.PEAK.FWHM,
                1.0 * CONSTS.SPECTRA.PEAK.FWHM,
                1.0 * CONSTS.SPECTRA.PEAK.FWHM,
                0.5 * CONSTS.SPECTRA.PEAK.FWHM,
                0.5 * CONSTS.SPECTRA.PEAK.FWHM,
                0.5 * CONSTS.SPECTRA.PEAK.FWHM
            ]
        )

        # local maxima + poly2 approx
        analyzer = SpectrumAnalyzer(
            rps=rps.LocalMaximaRPS(),
            pps=pps.Poly2ApproximationPPS(),
            matcher=None,
            parameters=params
        )

        analyzer.update(xs, ys)
        plt.vlines(analyzer.xs_max, ymin=-30, ymax=10, colors="red")

        plt.show()

    elif load_from == "interrogator":
        from Interrogator.Interrogator import FS22SI

        interrogator = FS22SI(
            parent=None,
            host="10.6.1.93",
            port=3500,
            long_timeout=1000,
            short_timeout=5
        )

        interrogator.acqu_channel(7)

        plt.plot(interrogator.x, interrogator.y)

        # params
        params = {}
        params["peak_number"] = 1
        params["ban_widths"] = np.full(shape=1, fill_value=CONSTS.SPECTRA.INTERPEAK_DISTANCE / 2)
        params["fwhms"] = np.array(
            [
                1.0 * CONSTS.SPECTRA.PEAK.FWHM
            ]
        )

        # local maxima + poly2 approx
        analyzer = SpectrumAnalyzer(
            rps=rps.LocalMaximaRPS(),
            pps=pps.Poly2ApproximationPPS(),
            matcher=None,
            parameters=params
        )

        analyzer.update(interrogator.x, interrogator.y)
        plt.vlines(analyzer.xs_max, ymin=-60, ymax=-10, colors="red")
        for x_max in analyzer.xs_max:
            plt.text(x_max, -10, "{0}".format(x_max))

        plt.show()
