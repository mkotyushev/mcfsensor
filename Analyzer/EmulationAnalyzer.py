from Analyzer.Analyzer import Analyzer

from itertools import cycle
import numpy as np


class EmulationAnalyzer(Analyzer):
    def __init__(self, file_names, parent=None):
        super().__init__(parent)

        self.file_names = file_names
        self.file_names_iter = cycle(self.file_names)

        self.data = []

    def update(self):
        file_name = next(self.file_names_iter)
        print(file_name)
        self.data = np.loadtxt(file_name, dtype=float, delimiter=" ")
        self.updated.emit(self.data)
