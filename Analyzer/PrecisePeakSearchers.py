import numpy as np


class CentroidPPS:
    pass


class Poly2ApproximationPPS:  # precise peak searcher
    def __call__(self, xs, ys):
        approximation = np.poly1d(np.polyfit(xs, ys, 2))

        a, b, c = approximation.coeffs
        x_max = - b / (2 * a)
        y_max = a * x_max ** 2 + b * x_max + c

        return x_max, y_max
