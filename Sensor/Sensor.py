from PyQt5.QtCore import QObject, pyqtSignal, QThread

from Sensor.FBG import FBG
from Sensor.Node import Node
from Sensor.Core import Core


class Sensor(QObject):
    updated = pyqtSignal(object)

    def __init__(self, nodes_configs, cores_configs, fbgs_configs, parent=None):
        super().__init__(parent)

        self.fbgs = {
            fgb_number:
                FBG(fbg_config)
                for fgb_number, fbg_config in fbgs_configs.items()
        }
        self.cores = {
            core_number:
                Core({order_number: self.fbgs[fbg_number] for order_number, fbg_number in core_config["fbgs"].items()})
                for core_number, core_config in cores_configs.items()
        }
        self.nodes = {
            node_number:
                Node(self.fbgs[node_config["fbg_t"]], self.fbgs[node_config["fbg_1"]], self.fbgs[node_config["fbg_2"]])
                for node_number, node_config in nodes_configs.items()
        }

    def update(self, channel_number, xs_max, ys_max):
        print(int(QThread.currentThreadId()), __name__)

        if channel_number not in self.cores:
            return

        for i in range(len(xs_max)):
            if i not in self.cores[channel_number].fbgs:
                return
            self.cores[channel_number].fbgs[i].update(xs_max[i])

        for node_number, node in self.nodes.items():
            node.update()

        self.updated.emit(self.nodes)


if __name__ == "__main__":
    pass
