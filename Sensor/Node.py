from PyQt5.QtCore import QObject
from math import tan, sin, atan, cos


class Node(QObject):
    def __init__(self, fbg_t, fbg_1, fbg_2, parent=None):
        super().__init__(parent)

        self.fbg_t = fbg_t
        self.fbg_1 = fbg_1
        self.fbg_2 = fbg_2

        self.r_c = 0
        self.phi_c = 0
        self.offset = fbg_t.offset

    def update(self):
        # Pseudonyms for fbgs to increase readability
        fbg_1, fbg_2 = self.fbg_1, self.fbg_2

        # Temporary variables
        delta_wavelength_1 = fbg_1.wavelength - fbg_1.bragg_wavelength
        delta_wavelength_2 = fbg_2.wavelength - fbg_2.bragg_wavelength

        # Check if fbgs are unstrained (zero-division case)
        if delta_wavelength_1 == 0 and delta_wavelength_2 == 0:
            self.r_c = 0
            self.phi_c = 0
            return

        # Check if only one of the fbgs is shifted
        if delta_wavelength_1 == 0 and delta_wavelength_2 != 0:
            self.r_c = fbg_2.sens_strain * fbg_2.r / delta_wavelength_2
            self.phi_c = fbg_2.phi
            return

        if delta_wavelength_1 != 0 and delta_wavelength_2 == 0:
            self.r_c = fbg_1.sens_strain * fbg_1.r / delta_wavelength_1
            self.phi_c = fbg_1.phi
            return

        # Temporary variables
        a = (fbg_1.sens_strain * delta_wavelength_1 * fbg_1.r) / (fbg_2.sens_strain * delta_wavelength_2 * fbg_2.r)
        b = fbg_1.sens_strain * fbg_1.r / delta_wavelength_1
        phi_1_2 = fbg_2.phi - fbg_1.phi
        tan_phi_c = 1.0 / tan(phi_1_2) - a / sin(phi_1_2)

        self.phi_c = atan(tan_phi_c)
        self.r_c = b / (1 + tan_phi_c ** 2) ** (1 / 2)


if __name__ == "__main__":
    pass
