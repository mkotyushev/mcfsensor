from PyQt5.QtCore import QObject


class FBG(QObject):
    def __init__(self, config, parent=None):
        super().__init__(parent)

        self.bragg_wavelength = config["bragg_wavelength"]
        self.sens_therm = config["sens_therm"]
        self.sens_strain = config["sens_strain"]

        self.offset = config["offset"]
        self.r = config["r"]
        self.phi = config["phi"]

        self.strain = 0
        self.therm = 0
        self.wavelength = self.bragg_wavelength

    def update(self, wavelength):
        self.wavelength = wavelength

        delta_wavelength = self.wavelength - self.bragg_wavelength

        self.strain = delta_wavelength * self.sens_strain
        self.therm = delta_wavelength * self.sens_therm


if __name__ == "__main__":
    pass
