import json

from PyQt5.QtCore import QObject


class Config(QObject):
    def __init__(self, file_name, parent=None):
        super().__init__(parent)

        f = open(file_name)
        self.dict = json.load(f)
        f.close()

        self.fbgs_configs = string_keys_to_int(self.dict["fbgs"])
        self.nodes_configs = string_keys_to_int(self.dict["nodes"])
        self.cores_configs = string_keys_to_int(self.dict["cores"])


def string_keys_to_int(dictionary):
    if type(dictionary) is not dict:
        return dictionary

    result = {}
    for key, value in dictionary.items():
        if key.isdigit():
            result[int(key)] = string_keys_to_int(value)
        else:
            result[key] = string_keys_to_int(value)

    return result

