import sympy

k1, k2, x, p = sympy.symbols("k1 k2 x p")

sol = sympy.solve(k2 - k1 * (1 - x / p) / (1 + k1 ** 2 * x ** 2 * (1 - x / (2 * p))) ** (3 / 2), x)

print(sol)
