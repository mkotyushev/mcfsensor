import sys
from PyQt5.QtCore import QTime, QObject, QThread
from PyQt5.QtNetwork import QTcpSocket

import numpy as np
import matplotlib.pyplot as plt
from PyQt5.QtWidgets import QApplication, QMainWindow


class SyncSocket(QTcpSocket):
    def __init__(self,
                 parent: QObject = None,
                 host: str = "127.0.0.1",
                 port: int = 22,
                 short_timeout=50,
                 long_timeout=1000):
        super().__init__(parent=parent)

        self.host = host
        self.port = port

        self.short_timeout = short_timeout
        self.long_timeout = long_timeout

        self.start()

    def restart(self):
        self.stop()
        self.start()

    def start(self):
        self.connectToHost(self.host, self.port)
        self.waitForConnected(self.long_timeout)

    def stop(self):
        self.disconnectFromHost()
        self.waitForDisconnected(self.long_timeout)

    def receive(self) -> str:
        time = QTime()
        time.start()

        data = ""
        while time.elapsed() <= self.long_timeout:
            if not self.waitForReadyRead(self.short_timeout):
                break

            line = str(bytes(self.readAll()).decode())

            if line == "":
                break

            data += line

        return data

    def send(self, message: str) -> None:
        self.write(message.encode())
        self.flush()
        self.waitForBytesWritten(self.long_timeout)


class Interrogator(QObject):
    def __init__(self,
                 parent=None,
                 host="10.6.1.10",
                 command_port=3500,
                 data_port=3365,
                 short_timeout=500,
                 long_timeout=1000):
        super().__init__(parent=parent)

        self.command_socket = SyncSocket(parent=self, host=host, port=command_port, short_timeout=short_timeout, long_timeout=long_timeout)
        self.data_socket = SyncSocket(parent=self, host=host, port=data_port, short_timeout=short_timeout, long_timeout=long_timeout)

        self.x = np.linspace(1500, 1600, 20001)
        self.y = np.linspace(1500, 1600, 20001)

    def acquise(self, channel: int = 0):
        self.command_socket.send(":ACQU:OSAT:CHAN:{0}?\r\n".format(channel))
        data = self.command_socket.receive()

        self.y = np.array(data[len(":ACK:"):].rstrip().split(","), dtype=np.float32)

        self.command_socket.restart()

    def acquise_all(self):
        pass


if __name__ == "__main__":
    irg = Interrogator()

    thread = QThread()
    irg.moveToThread(thread)
    thread.start()

    irg.acquise(0)

    app = QApplication(sys.argv)
    window = QMainWindow()
    window.show()
    sys.exit(app.exec_())
