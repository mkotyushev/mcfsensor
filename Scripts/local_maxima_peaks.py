import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

import Analyzer.Spectra as spectra


class CONSTS:
    class DRAWING:
        WIDTH = 1e-9
        HEIGHT = 1

    class SPECTRA:
        INTERPEAK_DISTANCE = 4e-9

        class PEAK:
            FWHM = 3e-10


path = "../../data/current/"
filenames = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

# filename = filenames[0]
for filename in filenames:
    patches = []
    data = np.loadtxt(path + filename, delimiter=' ')
    xs, ys = data[0], data[1]
    patches.append(mpatches.Patch(color='green', label='Spectrum'))
    plt.plot(xs, ys, 'g', linewidth=3)

    # find the maxima as local maxima (a low accuracy)
    widths = np.full(shape=9, fill_value=CONSTS.SPECTRA.INTERPEAK_DISTANCE / 2)
    indices_max = spectra.maxima_indices(xs, ys, 9, widths)
    # for x_max, y_max in zip(xs[indices_max], ys[indices_max]):
    #     plt.hlines(y_max, xmin=x_max - CONSTS.DRAWING.WIDTH, xmax=x_max + CONSTS.DRAWING.WIDTH, colors='r')
    #     plt.vlines(x_max, ymin=y_max - CONSTS.DRAWING.HEIGHT, ymax=y_max + CONSTS.DRAWING.HEIGHT, colors='r')
    # patches.append(mpatches.Patch(color='red', label='Peaks w/ local maxima algo'))

    # find approximation in areas
    widths = np.array(
        [
            2.0 * CONSTS.SPECTRA.PEAK.FWHM,
            2.0 * CONSTS.SPECTRA.PEAK.FWHM,
            2.0 * CONSTS.SPECTRA.PEAK.FWHM,
            0.7 * CONSTS.SPECTRA.PEAK.FWHM,
            0.7 * CONSTS.SPECTRA.PEAK.FWHM,
            0.7 * CONSTS.SPECTRA.PEAK.FWHM,
            0.5 * CONSTS.SPECTRA.PEAK.FWHM,
            0.5 * CONSTS.SPECTRA.PEAK.FWHM,
            0.5 * CONSTS.SPECTRA.PEAK.FWHM
        ]
    )
    for x_rude, width in zip(xs[indices_max], widths):
        approximation = spectra.approx(xs, ys, x_rude - width, x_rude + width)

        a, b, c = approximation.coeffs
        x_max = - b / (2 * a)
        y_max = a * x_max ** 2 + b * x_max + c

        mask = (xs >= (x_max - width)) & (xs < (x_max + width))
        xs_app = xs[mask]
        ys_app = a * xs_app ** 2 + b * xs_app + c
        plt.plot(xs_app, ys_app, 'k')

        plt.hlines(y_max, xmin=x_max - CONSTS.DRAWING.WIDTH, xmax=x_max + CONSTS.DRAWING.WIDTH, colors='b')
        plt.vlines(x_max, ymin=y_max - CONSTS.DRAWING.HEIGHT, ymax=y_max + CONSTS.DRAWING.HEIGHT, colors='b')
    patches.append(mpatches.Patch(color='blue', label='Peaks w/ polynomial approximation'))

    plt.legend(handles=patches)
    plt.show()
