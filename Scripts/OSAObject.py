from PyQt5 import QtCore
from PyQt5.QtCore import QObject
import numpy as np

from OSA.OSA import OSA_AQ6370

def decodeOSADataToNpArray(data_bytes):
    data_str = data_bytes.decode(errors='ignore')
    data = np.array(data_str.strip().split(',')).astype(np.float)
    return data

class OSAObject(QObject):
    leftSpectrumReceived = QtCore.pyqtSignal(np.ndarray, np.ndarray)
    rightSpectrumReceived = QtCore.pyqtSignal(np.ndarray, np.ndarray)
    def __init__(self, host, port, trace):
        QObject.__init__(self)
        self.osa = OSA_AQ6370(host=host, port=port, trace=trace)
    def getData(self, center, span):
        return self.osa.PullSpectrum(center, span)
    def getLeftData(self, center=1575, span=80):
        x_bytes, y_bytes = self.getData(center, span)
        x = decodeOSADataToNpArray(x_bytes)
        y = decodeOSADataToNpArray(y_bytes)
        self.leftSpectrumReceived.emit(x, y)
    def getRightData(self, center=1575, span=80):
        x_bytes, y_bytes = self.getData(center, span)
        x = decodeOSADataToNpArray(x_bytes)
        y = decodeOSADataToNpArray(y_bytes)
        self.rightSpectrumReceived.emit(x, y)
