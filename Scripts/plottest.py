# -*- coding: utf-8 -*-
"""
Demonstrate use of GLLinePlotItem to draw cross-sections of a surface.

"""
## Add path to library (just for examples; you do not need this)
from PyQt5 import QtCore, QtGui
import pyqtgraph.opengl as gl
import pyqtgraph as pg
import numpy as np
from PyQt5.QtWidgets import QMainWindow

from UIs.MainWindowUI import Ui_MainWindowUI


class MW(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindowUI()
        self.ui.setupUi(self)

app = QtGui.QApplication([])

w1 = MW()
w1.show()

w = gl.GLViewWidget(w1)
w.opts['distance'] = 40
w.show()
w.setWindowTitle('pyqtgraph example: GLLinePlotItem')

w1.ui.plotLayout.addWidget(w)

gx = gl.GLGridItem()
gx.rotate(90, 0, 1, 0)
gx.translate(-10, 0, 0)
w.addItem(gx)
gy = gl.GLGridItem()
gy.rotate(90, 1, 0, 0)
gy.translate(0, -10, 0)
w.addItem(gy)
gz = gl.GLGridItem()
gz.translate(0, 0, -10)
w.addItem(gz)


def fn(x, y):
    return np.cos((x ** 2 + y ** 2) ** 0.5)


n = 51
y = np.linspace(-10, 10, n)
x = np.linspace(-10, 10, 100)
for i in range(n):
    yi = np.array([y[i]] * 100)
    d = (x ** 2 + yi ** 2) ** 0.5
    z = 10 * np.cos(d) / (d + 1)
    pts = np.vstack([x, yi, z]).transpose()
    plt = gl.GLLinePlotItem(pos=pts, color=pg.glColor((i, n * 1.3)), width=(i + 1) / 10., antialias=True)
    w.addItem(plt)

## Start Qt event loop unless running in interactive mode.
if __name__ == '__main__':
    import sys

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
