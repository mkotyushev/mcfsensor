import sys
import traceback

from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QMainWindow, QApplication, QTabWidget
from PyQt5.QtChart import QChart, QChartView, QLineSeries

from Scripts.new_interface.UIMainWindow import Ui_MainWindow
from Visualization.PainterPyQtGraph import PainterPyQtGraph


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.painter = PainterPyQtGraph(self)
        self.ui.gridLayout_3DPlot.addWidget(self.painter)

        self.init_ui()

    def init_ui(self):
        core_count = 7
        # добавить строк в таблицы по числу сердцевин
        for i in range(core_count):
            self.ui.tableWidget_wavelength_all.insertRow(self.ui.tableWidget_wavelength_all.rowCount())
        self.ui.tableWidget_wavelength_all.insertColumn(self.ui.tableWidget_wavelength_all.columnCount())
        for i in range(core_count):
            self.ui.tableWidget_wavelength_selected.insertRow(self.ui.tableWidget_wavelength_selected.rowCount())
        self.ui.tableWidget_wavelength_selected.insertColumn(self.ui.tableWidget_wavelength_selected.columnCount())
        # добавить табов для спектров по числу сердцевин
        for i in range(core_count):
            chart = QChart()
            line = QLineSeries()
            line.append(0, 1)
            line.append(1, 2)
            line.append(2, 1)
            line.append(3, 4)
            line.append(4, 6)
            line.append(5, 12)

            с = (255 * i) // core_count
            line.setColor(QColor(
                с if i % 3 == 0 else 0,
                с if i % 3 == 1 else 0,
                с if i % 3 == 2 else 0))

            chart.addSeries(line)
            tab = QChartView(chart, self.ui.tabWidget_spectra)
            # TODO: добавить в табы связь с соответствующим спектром
            self.ui.tabWidget_spectra.insertTab(self.ui.tabWidget_spectra.count(), tab, "Core {0}".format(i + 1))
        # синхронизовать выбор сердцевины на трех виджетах
        self.ui.tableWidget_wavelength_all.cellClicked.connect(self.ui.tableWidget_wavelength_selected.setCurrentCell)
        self.ui.tableWidget_wavelength_all.cellClicked.connect(
            lambda x, y: self.ui.tabWidget_spectra.setCurrentIndex(x))
        self.ui.tableWidget_wavelength_selected.cellClicked.connect(self.ui.tableWidget_wavelength_all.setCurrentCell)
        self.ui.tableWidget_wavelength_selected.cellClicked.connect(
            lambda x, y: self.ui.tabWidget_spectra.setCurrentIndex(x))
        self.ui.tabWidget_spectra.currentChanged.connect(
            lambda x: self.ui.tableWidget_wavelength_all.setCurrentCell(x, 0))
        self.ui.tabWidget_spectra.currentChanged.connect(
            lambda x: self.ui.tableWidget_wavelength_selected.setCurrentCell(x, 0))

if __name__ == "__main__":
    try:
        app = QApplication(sys.argv)
        window = MainWindow()
        window.show()
        sys.exit(app.exec_())
    except Exception as e:
        traceback.print_exc()
