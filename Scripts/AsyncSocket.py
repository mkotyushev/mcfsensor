import sys
from PyQt5.QtCore import QObject, pyqtSignal, QThread
from PyQt5.QtNetwork import QTcpSocket
from PyQt5.QtWidgets import QApplication, QMainWindow


class AsyncSocket(QTcpSocket):
    received = pyqtSignal(object)

    def __init__(self,
                 parent: QObject = None,
                 host: str = "127.0.0.1",
                 port: int = 22,
                 short_timeout=50,
                 long_timeout=1000):
        super().__init__(parent=parent)

        self.host = host
        self.port = port
        self.long_timeout = long_timeout
        self.short_timeout = short_timeout

        # self.buffer = ""
        self.request_queue = []

        self.readyRead.connect(self.on_ready_read)

        self.connectToHost(self.host, self.port)
        self.waitForConnected(self.long_timeout)

    def send(self, request: str) -> None:
        self.request_queue.append(request)

        self.write(request.encode())
        self.flush()

    def on_ready_read(self):
        while not self.canReadLine():
            pass
        line = str(bytes(self.readLine()).decode())
        print(line)

        # if line.find("\r\n") == -1:
        #     self.buffer += line
        #     return
        #
        # answers = line.split("\r\n")
        #
        # try:
        #     answer_pairs = [(self.request_queue.pop(0), self.buffer + answers[0])]
        #
        #     for request, answer in zip(self.request_queue[1:1 + len(answers[1:-1])], answers[1:-1]):
        #         answer_pairs.append((request, answer))
        #
        #     self.buffer = answers[-1]
        #     self.received.emit(answer_pairs)
        # except IndexError:
        #     print("Wrong request-answer sequencing.")


if __name__ == "__main__":
    thread = QThread()
    socket = AsyncSocket(parent=None, host="10.6.1.10", port=3500, short_timeout=500, long_timeout=1000)
    socket.moveToThread(thread)
    thread.start()

    socket.send(":ACQU:OSAT:CHAN:0?\r\n")

    app = QApplication(sys.argv)
    window = QMainWindow()
    window.show()
    sys.exit(app.exec_())
