"""
Установить пакеты:
    pyqt5
"""

import sys
import traceback

from PyQt5.QtCore import QThread, QObject, pyqtSignal, QTimer
from PyQt5.QtWidgets import QMainWindow, QApplication


class Image(QObject):
    pass


class Camera(QObject):
    shot = pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.image = Image()
        self.i = 0

    def shoot(self):
        # your code here
        print("I'm a camera and I shooting my {0}'s image".format(self.i))
        self.i += 1

        self.shot.emit(self.image)


class ImageProcessor(QObject):
    processed = pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.image = Image()
        self.i = 0

    def process(self, image):
        self.image = image

        # your code here
        print("I'm an image processor and I processing my {0}'s image".format(self.i))
        self.i += 1

        self.processed.emit(self.image)


class ImageViewer(QObject):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.image = Image()
        self.i = 0

    def view(self, image):
        self.image = image

        # your code here
        print("I'm an image viewer and I showing my {0}'s image".format(self.i))
        self.i += 1


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.viewer_thread = QThread()
        self.viewer = ImageViewer()
        self.viewer.moveToThread(self.viewer_thread)
        self.viewer_thread.start()

        self.processor_thread = QThread()
        self.processor = ImageProcessor()
        self.processor.processed.connect(self.viewer.view)
        self.processor.moveToThread(self.processor_thread)
        self.processor_thread.start()

        self.camera_thread = QThread()
        self.camera = Camera()
        self.camera.shot.connect(self.processor.process)
        self.camera.moveToThread(self.camera_thread)
        self.camera_thread.start()

        self.timer = QTimer()
        self.timer.setInterval(2500)
        self.timer.timeout.connect(self.camera.shoot)
        self.timer.start()


if __name__ == '__main__':
    try:
        app = QApplication(sys.argv)
        window = MainWindow()
        window.show()
        sys.exit(app.exec_())
    except Exception as e:
        traceback.print_exc()
