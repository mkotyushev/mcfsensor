# -*- coding: utf-8 -*-

import sys
import traceback
from PyQt5.QtWidgets import QApplication

from Windows.MainWindow import MainWindow


if __name__ == '__main__':
    try:
        app = QApplication(sys.argv)
        window = MainWindow()
        window.show()
        sys.exit(app.exec_())
    except Exception as e:
        traceback.print_exc()
