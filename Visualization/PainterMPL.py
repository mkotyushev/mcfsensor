from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from Visualization.Painter import Painter


class PainterMPL(Painter):
    def __init__(self, parent=None):
        """
        Constructs PainterMPL.
        :param parent: QWidget
        """
        super().__init__(parent=parent)

        self.figure = Figure()

        self.canvas = FigureCanvas(self.figure)
        self.canvas.setParent(parent)

        self.ax = self.figure.add_subplot(111, projection='3d')

    def update_data(self, points):
        """
        Updates plot replacing old points with new.
        :param points: list of QVector3D
        :return None:
        """
        self.ax.clear()

        x = [point.x() for point in points]
        y = [point.y() for point in points]
        z = [point.z() for point in points]
        self.ax.plot(x, y, z, '*-')

        # refresh canvas
        self.canvas.draw()

if __name__ == "__main__":
    pass
