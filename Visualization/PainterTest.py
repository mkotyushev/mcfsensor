import sys

from PyQt5.QtCore import QTimer
from PyQt5.QtGui import QVector3D
from PyQt5.QtWidgets import QApplication
from math import pi
from Utils.Extras3D import rotate, get_circle_segment_3d

from PyQt5.QtWidgets import QMainWindow
from UIs.MainWindowUI import Ui_MainWindowUI


class MW(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.ui = Ui_MainWindowUI()
        self.ui.setupUi(self)

app = QApplication(sys.argv)
window = MW()
test = "PainterPyQtGraph"

if test == "PainterPyQtGraph":
    from Visualization.PainterPyQtGraph import PainterPyQtGraph
    painter = PainterPyQtGraph(window)
elif test == "PainterMPL":
    from Visualization.PainterMPL import PainterMPL
    painter = PainterMPL(window)
elif test == "PainterQt3D":
    from Visualization.PainterQt3D import PainterQt3D
    painter = PainterQt3D(window)
else:
    painter = None

window.ui.plotLayout.addWidget(painter)


def f():
    f.c = (f.c + 1) % 255 + 1

    bp = QVector3D(0.0, 0.0, 0.0)
    md = QVector3D(0.0, -1.0, 0.0)
    cd = QVector3D(0.0, 0.0, 1.0)
    l = 5
    r = 10

    r = 2 * pi * r * f.c / 1024
    cd = rotate(cd, md, pi / 100)
    points, moving_direction, center_direction = get_circle_segment_3d(
        bp,
        md,
        cd,
        l,
        r)

    painter.update_data(points)

f.c = 0

timer = QTimer()
timer.setInterval(50)
timer.timeout.connect(f)
timer.start()

window.show()
sys.exit(app.exec_())
