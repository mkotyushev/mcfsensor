import pyqtgraph.opengl as gl
import pyqtgraph as pg
import numpy as np
from PyQt5.QtCore import QThread


class PainterPyQtGraph(gl.GLViewWidget):
    """
    Class providing QObject wrapper for 3D painting.
    """
    def __init__(self, parent=None):
        """
        Constructs Painter.
        """
        super().__init__(parent)

        self.opts['distance'] = 40
        self.show()
        self.setWindowTitle('pyqtgraph example: GLLinePlotItem')
        self.draw_grid()

    def update_data(self, points):
        """
        Updates plot replacing old points with new.
        :param points: list of QVector3D
        :return None:
        """
        print(int(QThread.currentThreadId()), __name__)

        x = [point.x() for point in points]
        y = [point.y() for point in points]
        z = [point.z() for point in points]
        pts = np.vstack([x, y, z]).transpose()

        self.clear()
        plt = gl.GLLinePlotItem(pos=pts, color=pg.glColor((1, 150)), width=3, antialias=True)
        self.addItem(plt)
        self.draw_grid()

    def draw_grid(self):
        gx = gl.GLGridItem()
        gx.rotate(90, 0, 1, 0)
        gx.scale(0.1, 0.1, 1)
        gx.translate(0, 1, 1)
        self.addItem(gx)

        gy = gl.GLGridItem()
        gy.rotate(90, 1, 0, 0)
        gy.scale(0.1, 0.1, 1)
        gy.translate(1, 0, 1)
        self.addItem(gy)

        gz = gl.GLGridItem()
        gz.scale(0.1, 0.1, 1)
        gz.translate(1, 1, 0)
        self.addItem(gz)

    def clear(self):
        """
        Remove all items from the scene.
        """
        for item in self.items:
            item._setView(None)
        self.items = []

    def keyPressEvent(self, event):
        print("Key was pressed", self.__class__.__name__)


if __name__ == "__main__":
    pass
