from PyQt5.QtWidgets import QWidget


class Painter(QWidget):
    """
    Class providing QObject wrapper for 3D painting.
    """
    def __init__(self, parent=None):
        """
        Constructs Painter.
        :param parent: QObject
        """
        super().__init__(parent=parent)

    def update(self, points):
        """
        Updates plot replacing old points with new.
        :param points: list of QVector3D
        :return None:
        """
        pass
