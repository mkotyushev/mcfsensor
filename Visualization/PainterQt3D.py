from PyQt5.QtWidgets import QWidget

from PyQt5.QtDataVisualization import Q3DScatter
from PyQt5.QtDataVisualization import QScatterDataProxy
from PyQt5.QtDataVisualization import QScatterDataItem
from PyQt5.QtDataVisualization import QScatter3DSeries
from PyQt5.QtDataVisualization import QAbstract3DSeries

from Visualization.Painter import Painter


class PainterQt3D(Painter):
    """
    Class providing QObject wrapper for 3D painting.
    """
    def __init__(self, layout, parent=None, lim_bot=-2.0, lim_top=2.0):
        """
        Constructs Painter.
        :param parent: QObject
        """
        super().__init__(parent=parent)

        # Create 3D plot window and add it as widget
        self.plot = Q3DScatter()
        plot_widget = QWidget.createWindowContainer(self.plot, parent=parent)

        # Add widget to layout
        layout.addWidget(plot_widget)

        # Freeze axes
        # self.freeze_axes(lim_bot, lim_top)

        # Set up plot visualities
        self.series = QScatter3DSeries()
        self.series.setMesh(QAbstract3DSeries.MeshSphere)
        self.series.setItemSize(0.1)
        self.plot.addSeries(self.series)

    def unfreeze_axes(self):
        """
        Sets auto adjustment of axes to True.
        :return: None
        """
        # Set up auto adjustment for all the axes
        for axis in [self.plot.axisX(), self.plot.axisY(), self.plot.axisZ()]:
            axis.setAutoAdjustRange(True)

    def freeze_axes(self, lim_bot, lim_top):
        """
        Sets auto adjustment of axes to False and sets axes limits.
        :param lim_bot: new min limit of all the axes
        :param lim_top: new max limit of all the axes
        :return: None
        """
        # Set up const min and max for plot axes
        for axis in [self.plot.axisX(), self.plot.axisY(), self.plot.axisZ()]:
            axis.setMin(lim_bot)
            axis.setMax(lim_top)
            axis.setAutoAdjustRange(False)

    def update_data(self, points):
        """
        Updates plot replacing old points with new.
        :param points: list of QVector3D
        :return None:
        """
        # Create new data proxy
        data_proxy = QScatterDataProxy()

        # Fill new data proxy with new points
        for point in points:
            data_proxy.addItem(QScatterDataItem(point))

        self.series.setDataProxy(data_proxy)

if __name__ == "__main__":
    pass
