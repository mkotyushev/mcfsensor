from PyQt5.QtCore import QObject, pyqtSignal, QThread
from PyQt5.QtGui import QVector3D

from Utils.Extras3D import get_circle_segment_3d, rotate


class Interpolator(QObject):
    updated = pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent=parent)

    def update(self, nodes):
        print(int(QThread.currentThreadId()), __name__)

        points = []

        _points = [QVector3D(0.0, 0.0, 0.0)]
        _moving_direction = QVector3D(0.0, 0.0, 1.0)
        _center_direction = QVector3D(1.0, 0.0, 0.0)

        prev_offset = 0
        for node_number, node in nodes.items():
            length = node.offset - prev_offset
            r = node.r_c if node.r_c != 0 else 100
            phi = node.phi_c
            _points, _moving_direction, _center_direction = get_circle_segment_3d(
                _points[-1],
                _moving_direction,
                rotate(_center_direction, _moving_direction, phi),
                2 if length == 0 else length,
                r)
            points += _points
            prev_offset = node.offset

        self.updated.emit(points)


if __name__ == "__main__":
    pass
