from PyQt5.QtCore import QSettings
from copy import copy

from PyQt5.QtWidgets import QDialog

"""
При инициализации нового модуля значения его настроек вынимаются из экемпляра QSettings.
Где брать дефолты? Нужен дополнительный пользовательский словарь.

Можно переписать загрузку из джсона на QSettings. Тогда класс конфига станет ненужен.

Делаем два .ini файла -- один общий, с настройками для разных штук. Другой -- для конфига сенсора.
Примерный вид:
[node1]
    ...
...
[nodeN]
    ...

[fbg1]
    ...
...
[fbgM]
    ...
"""

SETTINGS = {
    "test1/set1": 10,
    "test1/set2": 20,
    "set3": "hello"
}

settings = QSettings("setts.ini", QSettings.IniFormat)

# for key, value in SETTINGS.items():
#     print(settings.value(key, value))

for key, value in SETTINGS.items():
    settings.setValue(key, value)

settings.sync()


class A(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.settings_backup = QSettings(None)

a = A()


