from PyQt5.QtCore import QObject, QThread, pyqtSignal, pyqtSlot

import numpy as np

from Utils.SyncSocket import SyncSocket


class Interrogator(QObject):
    received = pyqtSignal(int, object, object)

    def __init__(self,
                 parent: QObject = None,
                 host: str = "10.6.1.10",
                 command_port: int = 3500,
                 data_port: int = 3365,
                 short_timeout: int = 500,
                 long_timeout: int = 1000):
        super().__init__(parent=parent)

        self.command_socket = SyncSocket(
            parent=self,
            host=host,
            port=command_port,
            short_timeout=short_timeout,
            long_timeout=long_timeout)
        self.data_socket = SyncSocket(
            parent=self,
            host=host,
            port=data_port,
            short_timeout=short_timeout,
            long_timeout=long_timeout)

        self.x = np.linspace(1500, 1600, 20001)
        self.y = [None for i in range(8)]

        self.last = 0
        self.number_of_channels = 8

    def acquise_continious(self):
        self.acquise(self.last)
        self.last = (self.last + 1) % self.number_of_channels

    def acquise(self, channel_num: int = 0) -> None:
        """
        Sends acquisition command for the single channel and receives the data.
        If the data was successful received emits 'received' signal.
        :param channel_num: int -- channel to acquise, range [0, 7]
        :return: None
        """
        print(int(QThread.currentThreadId()), __name__)

        # Record the channel number
        self.last = channel_num

        # Restart socket
        self.command_socket.restart()

        # Check channel number validity
        if channel_num not in range(7):
            return

        # Send acqu-request and receive the answer
        self.command_socket.send(":ACQU:OSAT:CHAN:{0}?\r\n".format(channel_num))
        data = self.command_socket.receive()

        # Parse answer in numpy array
        try:
            self.y[channel_num] = np.array(data[len(":ACK:"):].rstrip().split(","), dtype=np.float32)
            self.received.emit(channel_num, self.x, self.y[channel_num])
        except ValueError:
            return

    def acquise_all(self) -> None:
        """
        Sends acquisition command for all the channels and receives the data.
        If the data was successful received emits 'received_all' signal.
        :return: None
        """
        pass


if __name__ == "__main__":
    interrogator = Interrogator(
        parent=None,
        host="10.6.1.10",
        command_port=3500,
        data_port=3365,
        short_timeout=500,
        long_timeout=1000
    )
    thread = QThread()
    interrogator.moveToThread(thread)
    thread.start()

    import time
    t = time.time()
    interrogator.acquise(3)
    print("Single channel interrogation: {0}".format(time.time() - t))

    import matplotlib.pyplot as plt
    plt.plot(interrogator.x, interrogator.y[3])
    plt.show()
