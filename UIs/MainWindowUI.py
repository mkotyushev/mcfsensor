# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\MainWindowUI.ui'
#
# Created by: PyQt5 UI code generator 5.10
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1072, 736)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_spectra = QtWidgets.QGridLayout()
        self.gridLayout_spectra.setObjectName("gridLayout_spectra")
        self.tabWidget_spectra = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget_spectra.setObjectName("tabWidget_spectra")
        self.gridLayout_spectra.addWidget(self.tabWidget_spectra, 1, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_spectra, 0, 0, 1, 2)
        self.horizontalLayout_wavelength = QtWidgets.QHBoxLayout()
        self.horizontalLayout_wavelength.setObjectName("horizontalLayout_wavelength")
        self.tableWidget_wavelength_all = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget_wavelength_all.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tableWidget_wavelength_all.setDragDropMode(QtWidgets.QAbstractItemView.DropOnly)
        self.tableWidget_wavelength_all.setObjectName("tableWidget_wavelength_all")
        self.tableWidget_wavelength_all.setColumnCount(0)
        self.tableWidget_wavelength_all.setRowCount(0)
        self.horizontalLayout_wavelength.addWidget(self.tableWidget_wavelength_all)
        self.tableWidget_wavelength_selected = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget_wavelength_selected.setEditTriggers(QtWidgets.QAbstractItemView.NoEditTriggers)
        self.tableWidget_wavelength_selected.setDragDropMode(QtWidgets.QAbstractItemView.DragOnly)
        self.tableWidget_wavelength_selected.setObjectName("tableWidget_wavelength_selected")
        self.tableWidget_wavelength_selected.setColumnCount(0)
        self.tableWidget_wavelength_selected.setRowCount(0)
        self.horizontalLayout_wavelength.addWidget(self.tableWidget_wavelength_selected)
        self.gridLayout.addLayout(self.horizontalLayout_wavelength, 0, 2, 1, 1)
        self.gridLayout_settings = QtWidgets.QGridLayout()
        self.gridLayout_settings.setObjectName("gridLayout_settings")
        self.toolBox_settings = QtWidgets.QToolBox(self.centralwidget)
        self.toolBox_settings.setObjectName("toolBox_settings")
        self.page = QtWidgets.QWidget()
        self.page.setGeometry(QtCore.QRect(0, 0, 206, 320))
        self.page.setObjectName("page")
        self.toolBox_settings.addItem(self.page, "")
        self.page_2 = QtWidgets.QWidget()
        self.page_2.setGeometry(QtCore.QRect(0, 0, 206, 320))
        self.page_2.setObjectName("page_2")
        self.pushButton_interrogatorInterrogate = QtWidgets.QPushButton(self.page_2)
        self.pushButton_interrogatorInterrogate.setGeometry(QtCore.QRect(130, 0, 75, 23))
        self.pushButton_interrogatorInterrogate.setObjectName("pushButton_interrogatorInterrogate")
        self.comboBox_interrogatorChannelNumber = QtWidgets.QComboBox(self.page_2)
        self.comboBox_interrogatorChannelNumber.setGeometry(QtCore.QRect(0, 0, 121, 22))
        self.comboBox_interrogatorChannelNumber.setObjectName("comboBox_interrogatorChannelNumber")
        self.toolBox_settings.addItem(self.page_2, "")
        self.page_3 = QtWidgets.QWidget()
        self.page_3.setGeometry(QtCore.QRect(0, 0, 206, 320))
        self.page_3.setObjectName("page_3")
        self.toolBox_settings.addItem(self.page_3, "")
        self.gridLayout_settings.addWidget(self.toolBox_settings, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_settings, 1, 0, 1, 1)
        self.gridLayout_3DPlot = QtWidgets.QGridLayout()
        self.gridLayout_3DPlot.setObjectName("gridLayout_3DPlot")
        self.gridLayout.addLayout(self.gridLayout_3DPlot, 1, 1, 1, 2)
        self.gridLayout.setRowMinimumHeight(0, 130)
        self.gridLayout.setRowMinimumHeight(1, 200)
        self.gridLayout.setColumnStretch(0, 1)
        self.gridLayout.setColumnStretch(1, 1)
        self.gridLayout.setColumnStretch(2, 3)
        self.gridLayout.setRowStretch(0, 2)
        self.gridLayout.setRowStretch(1, 3)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1072, 21))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.tabWidget_spectra.setCurrentIndex(-1)
        self.toolBox_settings.setCurrentIndex(1)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.toolBox_settings.setItemText(self.toolBox_settings.indexOf(self.page), _translate("MainWindow", "Поиск пика"))
        self.pushButton_interrogatorInterrogate.setText(_translate("MainWindow", "Опросить"))
        self.toolBox_settings.setItemText(self.toolBox_settings.indexOf(self.page_2), _translate("MainWindow", "Интеррогатор"))
        self.toolBox_settings.setItemText(self.toolBox_settings.indexOf(self.page_3), _translate("MainWindow", "Калибровка"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

