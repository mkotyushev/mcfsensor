# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\parametersdialog.ui'
#
# Created by: PyQt5 UI code generator 5.10
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(630, 358)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(370, 310, 161, 31))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.schemeGV = QtWidgets.QGraphicsView(Dialog)
        self.schemeGV.setGeometry(QtCore.QRect(10, 10, 341, 331))
        self.schemeGV.setObjectName("schemeGV")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(370, 10, 111, 16))
        self.label.setObjectName("label")
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(370, 120, 75, 23))
        self.pushButton.setObjectName("pushButton")
        self.widget = QtWidgets.QWidget(Dialog)
        self.widget.setGeometry(QtCore.QRect(20, 310, 321, 25))
        self.widget.setObjectName("widget")
        self.schemeToolbar = QtWidgets.QHBoxLayout(self.widget)
        self.schemeToolbar.setContentsMargins(0, 0, 0, 0)
        self.schemeToolbar.setObjectName("schemeToolbar")
        self.topViewPB = QtWidgets.QPushButton(self.widget)
        self.topViewPB.setObjectName("topViewPB")
        self.schemeToolbar.addWidget(self.topViewPB)
        self.bottomViewPB = QtWidgets.QPushButton(self.widget)
        self.bottomViewPB.setObjectName("bottomViewPB")
        self.schemeToolbar.addWidget(self.bottomViewPB)
        self.frontViewPB = QtWidgets.QPushButton(self.widget)
        self.frontViewPB.setObjectName("frontViewPB")
        self.schemeToolbar.addWidget(self.frontViewPB)
        self.widget1 = QtWidgets.QWidget(Dialog)
        self.widget1.setGeometry(QtCore.QRect(370, 40, 251, 61))
        self.widget1.setObjectName("widget1")
        self.gratingParams = QtWidgets.QFormLayout(self.widget1)
        self.gratingParams.setContentsMargins(0, 0, 0, 0)
        self.gratingParams.setObjectName("gratingParams")
        self.label_4 = QtWidgets.QLabel(self.widget1)
        self.label_4.setObjectName("label_4")
        self.gratingParams.setWidget(0, QtWidgets.QFormLayout.LabelRole, self.label_4)
        self.portNumberComB = QtWidgets.QComboBox(self.widget1)
        self.portNumberComB.setObjectName("portNumberComB")
        self.gratingParams.setWidget(0, QtWidgets.QFormLayout.FieldRole, self.portNumberComB)
        self.label_2 = QtWidgets.QLabel(self.widget1)
        self.label_2.setObjectName("label_2")
        self.gratingParams.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label_2)
        self.wlLabel = QtWidgets.QLabel(self.widget1)
        self.wlLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.wlLabel.setObjectName("wlLabel")
        self.gratingParams.setWidget(1, QtWidgets.QFormLayout.FieldRole, self.wlLabel)
        self.label_3 = QtWidgets.QLabel(self.widget1)
        self.label_3.setObjectName("label_3")
        self.gratingParams.setWidget(2, QtWidgets.QFormLayout.LabelRole, self.label_3)
        self.ratioLabel = QtWidgets.QLabel(self.widget1)
        self.ratioLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.ratioLabel.setObjectName("ratioLabel")
        self.gratingParams.setWidget(2, QtWidgets.QFormLayout.FieldRole, self.ratioLabel)
        self.buttonBox.raise_()
        self.schemeGV.raise_()
        self.topViewPB.raise_()
        self.bottomViewPB.raise_()
        self.frontViewPB.raise_()
        self.label.raise_()
        self.label_2.raise_()
        self.label_3.raise_()
        self.pushButton.raise_()

        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.label.setText(_translate("Dialog", "Параметры решетки"))
        self.pushButton.setText(_translate("Dialog", "Калибровка"))
        self.topViewPB.setText(_translate("Dialog", "Сверху"))
        self.bottomViewPB.setText(_translate("Dialog", "Снизу"))
        self.frontViewPB.setText(_translate("Dialog", "В профиль"))
        self.label_4.setText(_translate("Dialog", "Номер порта интеррогатора"))
        self.label_2.setText(_translate("Dialog", "Длина волны, нм"))
        self.wlLabel.setText(_translate("Dialog", "0"))
        self.label_3.setText(_translate("Dialog", "K_изгиба, нм/рад"))
        self.ratioLabel.setText(_translate("Dialog", "0"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

